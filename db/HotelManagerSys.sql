USE [HotelManagerSys]
GO
/****** Object:  StoredProcedure [dbo].[usp_dishPrice]    Script Date: 2015-04-18 13:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_dishPrice]
(
	@year int,
	@month int
)
as
	select SUM([DishPrice]) as dishPrice from [dbo].[GuestRecord] where YEAR([LeaveDate])=@year and MONTH([LeaveDate])=@month
GO
/****** Object:  StoredProcedure [dbo].[usp_money]    Script Date: 2015-04-18 13:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_money]
(
	@year int,
	@month int
)
as
	select SUM([TotalMoney]) as totalMoneyfrom from [dbo].[GuestRecord] where YEAR([LeaveDate])=@year and MONTH([LeaveDate])=@month
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 2015-04-18 13:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[LoginId] [nvarchar](50) NOT NULL,
	[PassWord] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[LoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DishList]    Script Date: 2015-04-18 13:28:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DishList](
	[DishId] [int] IDENTITY(1,1) NOT NULL,
	[DishName] [nvarchar](50) NOT NULL,
	[Unit] [varchar](10) NOT NULL,
	[Price] [decimal](18, 0) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GuestRecord]    Script Date: 2015-04-18 13:28:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GuestRecord](
	[GuestId] [int] IDENTITY(1,1) NOT NULL,
	[GuestName] [nchar](20) NOT NULL,
	[Gender] [bit] NOT NULL,
	[IdentityId] [varchar](20) NOT NULL,
	[Phone] [nvarchar](11) NOT NULL,
	[RoomId] [int] NOT NULL,
	[ResideId] [int] NOT NULL,
	[ResideDate] [datetime] NOT NULL,
	[Deposit] [decimal](18, 0) NOT NULL,
	[TotalMoney] [decimal](18, 0) NULL,
	[DishPrice] [decimal](18, 0) NULL,
	[LeaveDate] [datetime] NULL,
	[TradeNo] [nvarchar](10) NULL,
 CONSTRAINT [PK_GuestRecord] PRIMARY KEY CLUSTERED 
(
	[GuestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ResideState]    Script Date: 2015-04-18 13:28:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResideState](
	[ResideId] [int] IDENTITY(1,1) NOT NULL,
	[ResideName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ResideState] PRIMARY KEY CLUSTERED 
(
	[ResideId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Room]    Script Date: 2015-04-18 13:28:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room](
	[RoomId] [int] IDENTITY(1,1) NOT NULL,
	[RoomTypeId] [int] NOT NULL,
	[RoomStateId] [int] NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
	[BedNum] [int] NOT NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoomState]    Script Date: 2015-04-18 13:28:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoomState](
	[RoomStateId] [int] IDENTITY(1,1) NOT NULL,
	[RoomStateName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_RoomState] PRIMARY KEY CLUSTERED 
(
	[RoomStateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoomType]    Script Date: 2015-04-18 13:28:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoomType](
	[TypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[TypePrice] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_RoomType] PRIMARY KEY CLUSTERED 
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Admin] ([LoginId], [PassWord]) VALUES (N'admin', N'21232F297A57A5A743894A0E4A801F')
SET IDENTITY_INSERT [dbo].[DishList] ON 

INSERT [dbo].[DishList] ([DishId], [DishName], [Unit], [Price]) VALUES (1, N'方便面', N'桶', CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[DishList] ([DishId], [DishName], [Unit], [Price]) VALUES (2, N'炒饭', N'份', CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[DishList] ([DishId], [DishName], [Unit], [Price]) VALUES (3, N'咖啡', N'杯', CAST(50 AS Decimal(18, 0)))
INSERT [dbo].[DishList] ([DishId], [DishName], [Unit], [Price]) VALUES (4, N'炸鸡', N'份', CAST(40 AS Decimal(18, 0)))
INSERT [dbo].[DishList] ([DishId], [DishName], [Unit], [Price]) VALUES (5, N'啤酒', N'听', CAST(8 AS Decimal(18, 0)))
INSERT [dbo].[DishList] ([DishId], [DishName], [Unit], [Price]) VALUES (6, N'鲜炸果汁', N'杯', CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[DishList] ([DishId], [DishName], [Unit], [Price]) VALUES (7, N'冷热牛奶', N'盒', CAST(15 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[DishList] OFF
SET IDENTITY_INSERT [dbo].[GuestRecord] ON 

INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (1, N'张三                  ', 0, N'519876537890985678', N'13098746783', 1, 2, CAST(0x0000A41A00C99250 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(120 AS Decimal(18, 0)), CAST(88 AS Decimal(18, 0)), CAST(0x0000A41A0106E3DB AS DateTime), N'388467801')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (2, N'李四                  ', 0, N'345654456789345670', N'12876547683', 3, 2, CAST(0x0000A41A00C9BC84 AS DateTime), CAST(50 AS Decimal(18, 0)), CAST(112 AS Decimal(18, 0)), CAST(99 AS Decimal(18, 0)), CAST(0x0000A47A00B1E323 AS DateTime), N'2070841645')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (3, N'王五                  ', 1, N'345687887984093892', N'12983647649', 3, 2, CAST(0x0000A41A00F20D55 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(112 AS Decimal(18, 0)), CAST(77 AS Decimal(18, 0)), CAST(0x0000A47A00B1E323 AS DateTime), N'2070841645')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (4, N'张的帅                 ', 0, N'985746536789098723', N'13876487594', 6, 2, CAST(0x0000A43900A2FB70 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(120 AS Decimal(18, 0)), CAST(66 AS Decimal(18, 0)), CAST(0x0000A43E00B1BC06 AS DateTime), N'388467804')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (5, N'张连接                 ', 0, N'123456678738749873', N'12378764789', 6, 2, CAST(0x0000A2DF00B8D01C AS DateTime), CAST(200 AS Decimal(18, 0)), CAST(120 AS Decimal(18, 0)), CAST(34 AS Decimal(18, 0)), CAST(0x0000A44D00B1BC06 AS DateTime), N'388467805')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (6, N'黎得芳                 ', 1, N'239485768758985769', N'13874657894', 9, 2, CAST(0x0000A45800B9292C AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(104 AS Decimal(18, 0)), CAST(88 AS Decimal(18, 0)), CAST(0x0000A45A00B4C073 AS DateTime), N'388467806')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (7, N'赵健康                 ', 0, N'123987345678098345', N'12345678909', 4, 2, CAST(0x0000A46C00BB9194 AS DateTime), CAST(200 AS Decimal(18, 0)), CAST(180 AS Decimal(18, 0)), CAST(166 AS Decimal(18, 0)), CAST(0x0000A47500BC6139 AS DateTime), N'388467807')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (8, N'Mike                ', 0, N'987465787567875678', N'12398746758', 3, 2, CAST(0x0000A45600BC9C78 AS DateTime), CAST(300 AS Decimal(18, 0)), CAST(112 AS Decimal(18, 0)), CAST(34 AS Decimal(18, 0)), CAST(0x0000A47A00B1E323 AS DateTime), N'2070841645')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (9, N'Nancy               ', 0, N'767676767676767676', N'45678765456', 7, 2, CAST(0x0000A47500BC9C78 AS DateTime), CAST(200 AS Decimal(18, 0)), CAST(360 AS Decimal(18, 0)), CAST(23 AS Decimal(18, 0)), CAST(0x0000A479017CE367 AS DateTime), N'388467809')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (10, N'赵豆腐                 ', 0, N'234847487567859856', N'23984765784', 6, 2, CAST(0x0000A47200CB5A34 AS DateTime), CAST(500 AS Decimal(18, 0)), CAST(120 AS Decimal(18, 0)), CAST(654 AS Decimal(18, 0)), CAST(0x0000A47900B1BC06 AS DateTime), N'388467810')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (11, N'刘大夫                 ', 0, N'345676567898789098', N'13546567876', 15, 2, CAST(0x0000A47500CF99A2 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(696 AS Decimal(18, 0)), CAST(23 AS Decimal(18, 0)), CAST(0x0000A479017CE988 AS DateTime), N'388467811')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (12, N'温赛道                 ', 1, N'39874656839847567', N'12345432345', 25, 2, CAST(0x0000A47500E5C878 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(320 AS Decimal(18, 0)), CAST(125 AS Decimal(18, 0)), CAST(0x0000A47D01175ACE AS DateTime), N'726942711')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (13, N'黎得芳                 ', 1, N'347656787578756787', N'13874656746', 18, 2, CAST(0x0000A46D00E62404 AS DateTime), CAST(50 AS Decimal(18, 0)), CAST(220 AS Decimal(18, 0)), NULL, CAST(0x0000A47900B354C6 AS DateTime), N'388467813')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (14, N'王麻子                 ', 0, N'348764657468749098', N'13876467654', 32, 2, CAST(0x0000A47900AB7609 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(112 AS Decimal(18, 0)), NULL, CAST(0x0000A47A00B44E16 AS DateTime), N'831842156')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (15, N'空间看                 ', 0, N'344874657874674678', N'13432345654', 9, 2, CAST(0x0000A46C00B3C70C AS DateTime), CAST(50 AS Decimal(18, 0)), CAST(104 AS Decimal(18, 0)), NULL, CAST(0x0000A47900B4C073 AS DateTime), N'388467815')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (16, N'张三                  ', 0, N'345456567876567876', N'13465465768', 3, 2, CAST(0x0000A46B00B58B64 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(1120 AS Decimal(18, 0)), NULL, CAST(0x0000A47A00B1E323 AS DateTime), N'2070841645')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (17, N'李四                  ', 0, N'134545676787890432', N'13454567656', 12, 2, CAST(0x0000A45800B58B64 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(297 AS Decimal(18, 0)), NULL, CAST(0x0000A47A00B22889 AS DateTime), N'1007152120')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (18, N'赵柳                  ', 0, N'342345654576567909', N'12312312312', 25, 2, CAST(0x0000A47900B5CCD0 AS DateTime), CAST(50 AS Decimal(18, 0)), CAST(320 AS Decimal(18, 0)), CAST(1659 AS Decimal(18, 0)), CAST(0x0000A47D01175ACE AS DateTime), N'726942711')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (19, N'走四方                 ', 0, N'345456787676567890', N'16567898767', 16, 2, CAST(0x0000A47300B5F3B0 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(140 AS Decimal(18, 0)), NULL, CAST(0x0000A47A00B85DDD AS DateTime), N'179853281')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (20, N'搞活动                 ', 0, N'4646464787489847', N'16567898767', 32, 2, CAST(0x0000A46B00B5F3B0 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(112 AS Decimal(18, 0)), CAST(123 AS Decimal(18, 0)), CAST(0x0000A47A00B44E16 AS DateTime), N'831842156')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (21, N'冠企鹅                 ', 1, N'237635467848794804', N'12343434345', 21, 2, CAST(0x0000A47200B6AF30 AS DateTime), CAST(100 AS Decimal(18, 0)), CAST(840 AS Decimal(18, 0)), CAST(666 AS Decimal(18, 0)), CAST(0x0000A479015CA41E AS DateTime), N'388467816')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (22, N'fgd                 ', 0, N'455445676767676767', N'12343456567', 7, 2, CAST(0x0000A45A017C7D14 AS DateTime), CAST(50 AS Decimal(18, 0)), CAST(600 AS Decimal(18, 0)), CAST(123 AS Decimal(18, 0)), CAST(0x0000A45A017CE367 AS DateTime), N'388467817')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (23, N'thytr               ', 0, N'455445676234576767', N'12343456765', 15, 2, CAST(0x0000A41F017C7D14 AS DateTime), CAST(50 AS Decimal(18, 0)), CAST(696 AS Decimal(18, 0)), CAST(44 AS Decimal(18, 0)), CAST(0x0000A41F017CE988 AS DateTime), N'388467818')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (24, N'eryw                ', 0, N'999445676234576767', N'12243456765', 27, 2, CAST(0x0000A433017C7D14 AS DateTime), CAST(50 AS Decimal(18, 0)), CAST(560 AS Decimal(18, 0)), CAST(54 AS Decimal(18, 0)), CAST(0x0000A43E017CED2C AS DateTime), N'388467819')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (25, N'眼睛突然                ', 0, N'444444444444444444', N'13434545654', 11, 2, CAST(0x0000A47B01166047 AS DateTime), CAST(30 AS Decimal(18, 0)), CAST(120 AS Decimal(18, 0)), CAST(34 AS Decimal(18, 0)), CAST(0x0000A47B011A4799 AS DateTime), N'1033389404')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (26, N'kljlk               ', 0, N'098987876789876789', N'12765678767', 1, 1, CAST(0x0000A479011C3F94 AS DateTime), CAST(100 AS Decimal(18, 0)), NULL, CAST(95 AS Decimal(18, 0)), NULL, NULL)
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (27, N'dff                 ', 1, N'333333333333333333', N'13444444444', 17, 2, CAST(0x0000A47A0120447C AS DateTime), CAST(40 AS Decimal(18, 0)), CAST(200 AS Decimal(18, 0)), CAST(1045 AS Decimal(18, 0)), CAST(0x0000A47C00F557E9 AS DateTime), N'1016384022')
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (28, N'wer                 ', 1, N'444455556666777789', N'13434567656', 11, 1, CAST(0x0000A47C00F5E031 AS DateTime), CAST(50 AS Decimal(18, 0)), NULL, CAST(0 AS Decimal(18, 0)), NULL, NULL)
INSERT [dbo].[GuestRecord] ([GuestId], [GuestName], [Gender], [IdentityId], [Phone], [RoomId], [ResideId], [ResideDate], [Deposit], [TotalMoney], [DishPrice], [LeaveDate], [TradeNo]) VALUES (29, N'ree                 ', 1, N'111122223333444456', N'14523409879', 19, 2, CAST(0x0000A47C00F5E031 AS DateTime), CAST(50 AS Decimal(18, 0)), CAST(200 AS Decimal(18, 0)), CAST(70 AS Decimal(18, 0)), CAST(0x0000A47D00A0D3D3 AS DateTime), N'423769823')
SET IDENTITY_INSERT [dbo].[GuestRecord] OFF
SET IDENTITY_INSERT [dbo].[ResideState] ON 

INSERT [dbo].[ResideState] ([ResideId], [ResideName]) VALUES (1, N'未结账')
INSERT [dbo].[ResideState] ([ResideId], [ResideName]) VALUES (2, N'已结账')
SET IDENTITY_INSERT [dbo].[ResideState] OFF
SET IDENTITY_INSERT [dbo].[Room] ON 

INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (1, 1, 1, N'1号房间', 2)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (2, 4, 2, N'2号房间', 4)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (3, 3, 2, N'3号房间', 2)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (4, 5, 2, N'4号房间', 6)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (5, 2, 2, N'5号房间', 3)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (6, 5, 2, N'6号房间', 6)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (7, 6, 2, N'7号房间', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (8, 6, 2, N'8号房间', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (9, 3, 2, N'9号房间', 2)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (10, 1, 2, N'10号房间', 2)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (11, 1, 1, N'11号房间', 2)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (12, 2, 2, N'12号房间', 3)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (13, 2, 2, N'13号房间', 3)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (14, 3, 2, N'14号房间', 4)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (15, 3, 2, N'15号房间', 5)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (16, 4, 2, N'16号房间', 4)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (17, 4, 2, N'17号房间', 4)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (18, 4, 2, N'18号房间', 4)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (19, 4, 2, N'19号房间', 4)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (20, 1, 2, N'20号房间', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (21, 1, 2, N'21号房间', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (22, 1, 2, N'22号房间', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (23, 2, 3, N'23号房间', 2)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (24, 2, 2, N'24号房间', 2)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (25, 3, 2, N'25号房间', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (26, 3, 2, N'26号房间', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (27, 3, 2, N'27号房间', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (28, 4, 2, N'28号房间', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (29, 4, 2, N'29号房间', 4)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (30, 5, 2, N'30号房间', 6)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (31, 11, 2, N'31号房', 1)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (32, 10, 2, N'32号房', 5)
INSERT [dbo].[Room] ([RoomId], [RoomTypeId], [RoomStateId], [Description], [BedNum]) VALUES (33, 2, 3, N'33号房', 2)
SET IDENTITY_INSERT [dbo].[Room] OFF
SET IDENTITY_INSERT [dbo].[RoomState] ON 

INSERT [dbo].[RoomState] ([RoomStateId], [RoomStateName]) VALUES (1, N'入住')
INSERT [dbo].[RoomState] ([RoomStateId], [RoomStateName]) VALUES (2, N'空闲')
INSERT [dbo].[RoomState] ([RoomStateId], [RoomStateName]) VALUES (3, N'维修')
SET IDENTITY_INSERT [dbo].[RoomState] OFF
SET IDENTITY_INSERT [dbo].[RoomType] ON 

INSERT [dbo].[RoomType] ([TypeId], [TypeName], [TypePrice]) VALUES (1, N'标准间', CAST(120 AS Decimal(18, 0)))
INSERT [dbo].[RoomType] ([TypeId], [TypeName], [TypePrice]) VALUES (2, N'长包房', CAST(90 AS Decimal(18, 0)))
INSERT [dbo].[RoomType] ([TypeId], [TypeName], [TypePrice]) VALUES (3, N'日租房', CAST(80 AS Decimal(18, 0)))
INSERT [dbo].[RoomType] ([TypeId], [TypeName], [TypePrice]) VALUES (4, N'小资客房', CAST(200 AS Decimal(18, 0)))
INSERT [dbo].[RoomType] ([TypeId], [TypeName], [TypePrice]) VALUES (5, N'总统套房', CAST(2000 AS Decimal(18, 0)))
INSERT [dbo].[RoomType] ([TypeId], [TypeName], [TypePrice]) VALUES (6, N'单人间', CAST(400 AS Decimal(18, 0)))
INSERT [dbo].[RoomType] ([TypeId], [TypeName], [TypePrice]) VALUES (10, N'豪华间', CAST(800 AS Decimal(18, 0)))
INSERT [dbo].[RoomType] ([TypeId], [TypeName], [TypePrice]) VALUES (11, N'经济适用房', CAST(100000 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[RoomType] OFF
ALTER TABLE [dbo].[GuestRecord] ADD  CONSTRAINT [DF_GuestRecord_DishPrice]  DEFAULT ((0)) FOR [DishPrice]
GO
ALTER TABLE [dbo].[Room] ADD  CONSTRAINT [DF_Room_RoomStateId]  DEFAULT ((2)) FOR [RoomStateId]
GO
ALTER TABLE [dbo].[GuestRecord]  WITH CHECK ADD  CONSTRAINT [FK_GuestRecord_ResideState] FOREIGN KEY([ResideId])
REFERENCES [dbo].[ResideState] ([ResideId])
GO
ALTER TABLE [dbo].[GuestRecord] CHECK CONSTRAINT [FK_GuestRecord_ResideState]
GO
ALTER TABLE [dbo].[GuestRecord]  WITH CHECK ADD  CONSTRAINT [FK_GuestRecord_Room] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Room] ([RoomId])
GO
ALTER TABLE [dbo].[GuestRecord] CHECK CONSTRAINT [FK_GuestRecord_Room]
GO
ALTER TABLE [dbo].[Room]  WITH CHECK ADD  CONSTRAINT [FK_Room_RoomState] FOREIGN KEY([RoomStateId])
REFERENCES [dbo].[RoomState] ([RoomStateId])
GO
ALTER TABLE [dbo].[Room] CHECK CONSTRAINT [FK_Room_RoomState]
GO
ALTER TABLE [dbo].[Room]  WITH CHECK ADD  CONSTRAINT [FK_Room_RoomType1] FOREIGN KEY([RoomTypeId])
REFERENCES [dbo].[RoomType] ([TypeId])
GO
ALTER TABLE [dbo].[Room] CHECK CONSTRAINT [FK_Room_RoomType1]
GO

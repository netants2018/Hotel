﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotelManager.BLL;
using HotelManager.Model;
using System.Data.SqlClient;
using HotelManager.Common;

namespace HotelManager.UI
{
    public partial class FrmAdmin : FrmBase
    {
        public FrmAdmin()
        {
            InitializeComponent();
            //用户名赋值
            this.txtLoginId.Enabled = false;
            this.txtLoginId.Text = Variable.LoginId;
        }

        //修改
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //非空
            foreach (Control var in this.Controls)
            {
                if (var is TextBox)
                {
                    if (string.IsNullOrEmpty(var.Text))
                    {
                        MessageBox.Show("请完善修改信息!","提示",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
            if (this.txtNewPwd.Text != this.txtAginNewPwd.Text)
            {
                MessageBox.Show("两次密码输入不一致!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                Admin admin = new Admin(this.txtLoginId.Text, this.txtNewPwd.Text);
                bool flag = AdminManager.UpdatePwd(admin);
                if (flag)
                {
                    MessageBox.Show("修改成功!","提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }
    }
}

﻿namespace HotelManager.UI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripButton tsbtnManager;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.菜单ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUpdatePwd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.tsbtnTypeMa = new System.Windows.Forms.ToolStripButton();
            this.tsbtnInfoMa = new System.Windows.Forms.ToolStripButton();
            this.tsbtnStatistics = new System.Windows.Forms.ToolStripButton();
            this.tsbtnGuestAdd = new System.Windows.Forms.ToolStripButton();
            this.tsmiGuestInfo = new System.Windows.Forms.ToolStripButton();
            this.tsbtnLock = new System.Windows.Forms.ToolStripButton();
            this.tsbtnExit = new System.Windows.Forms.ToolStripButton();
            this.tsState = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsTime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tslblDayOfWeek = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tsLoginId = new System.Windows.Forms.ToolStripLabel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtlv = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBad = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFree = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIn = new System.Windows.Forms.TextBox();
            this.txttype = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCount = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tvRoom = new System.Windows.Forms.TreeView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lvRooms = new System.Windows.Forms.ListView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtList = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.dgvRoom = new System.Windows.Forms.DataGridView();
            this.DishId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DishName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ch = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.skinEngine = new Sunisoft.IrisSkin.SkinEngine();
            tsbtnManager = new System.Windows.Forms.ToolStripButton();
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.tsState.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoom)).BeginInit();
            this.SuspendLayout();
            // 
            // tsbtnManager
            // 
            tsbtnManager.Image = global::HotelManager.UI.Properties.Resources.gif_47_052;
            tsbtnManager.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            tsbtnManager.ImageTransparentColor = System.Drawing.Color.Magenta;
            tsbtnManager.Name = "tsbtnManager";
            tsbtnManager.Size = new System.Drawing.Size(60, 96);
            tsbtnManager.Text = "餐饮维护";
            tsbtnManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            tsbtnManager.Click += new System.EventHandler(this.tsbtnManager_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.菜单ToolStripMenuItem,
            this.帮助ToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1030, 25);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // 菜单ToolStripMenuItem
            // 
            this.菜单ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiUpdatePwd,
            this.tsmiExit});
            this.菜单ToolStripMenuItem.Name = "菜单ToolStripMenuItem";
            this.菜单ToolStripMenuItem.Size = new System.Drawing.Size(59, 21);
            this.菜单ToolStripMenuItem.Text = "菜单(&S)";
            // 
            // tsmiUpdatePwd
            // 
            this.tsmiUpdatePwd.Name = "tsmiUpdatePwd";
            this.tsmiUpdatePwd.Size = new System.Drawing.Size(141, 22);
            this.tsmiUpdatePwd.Text = "修改密码(&U)";
            this.tsmiUpdatePwd.Click += new System.EventHandler(this.tsmiUpdatePwd_Click);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(141, 22);
            this.tsmiExit.Text = "退出(&E)";
            this.tsmiExit.Click += new System.EventHandler(this.tsmiExit_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAbout});
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(61, 21);
            this.帮助ToolStripMenuItem.Text = "帮助(&H)";
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            this.tsmiAbout.Size = new System.Drawing.Size(100, 22);
            this.tsmiAbout.Text = "关于";
            this.tsmiAbout.Click += new System.EventHandler(this.tsmiAbout_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.BackgroundImage = global::HotelManager.UI.Properties.Resources.banner;
            this.toolStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnTypeMa,
            this.tsbtnInfoMa,
            tsbtnManager,
            this.tsbtnStatistics,
            this.tsbtnGuestAdd,
            this.tsmiGuestInfo,
            this.tsbtnLock,
            this.tsbtnExit});
            this.toolStrip.Location = new System.Drawing.Point(0, 25);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip.Size = new System.Drawing.Size(1030, 99);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // tsbtnTypeMa
            // 
            this.tsbtnTypeMa.Image = global::HotelManager.UI.Properties.Resources.gif_47_064;
            this.tsbtnTypeMa.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnTypeMa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnTypeMa.Margin = new System.Windows.Forms.Padding(0);
            this.tsbtnTypeMa.Name = "tsbtnTypeMa";
            this.tsbtnTypeMa.Padding = new System.Windows.Forms.Padding(0, 15, 0, 15);
            this.tsbtnTypeMa.Size = new System.Drawing.Size(60, 99);
            this.tsbtnTypeMa.Text = "类型管理";
            this.tsbtnTypeMa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnTypeMa.Click += new System.EventHandler(this.tsbtnTypeMa_Click);
            // 
            // tsbtnInfoMa
            // 
            this.tsbtnInfoMa.Image = global::HotelManager.UI.Properties.Resources.gif_47_094;
            this.tsbtnInfoMa.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnInfoMa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnInfoMa.Name = "tsbtnInfoMa";
            this.tsbtnInfoMa.Size = new System.Drawing.Size(60, 96);
            this.tsbtnInfoMa.Text = "房间管理";
            this.tsbtnInfoMa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnInfoMa.Click += new System.EventHandler(this.tsbtnInfoMa_Click);
            // 
            // tsbtnStatistics
            // 
            this.tsbtnStatistics.Image = global::HotelManager.UI.Properties.Resources.gif_47_027;
            this.tsbtnStatistics.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnStatistics.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnStatistics.Margin = new System.Windows.Forms.Padding(0);
            this.tsbtnStatistics.Name = "tsbtnStatistics";
            this.tsbtnStatistics.Size = new System.Drawing.Size(60, 99);
            this.tsbtnStatistics.Text = "收入统计";
            this.tsbtnStatistics.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnStatistics.Click += new System.EventHandler(this.tsbtnStatistics_Click);
            // 
            // tsbtnGuestAdd
            // 
            this.tsbtnGuestAdd.Image = global::HotelManager.UI.Properties.Resources.gif_47_030;
            this.tsbtnGuestAdd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnGuestAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnGuestAdd.Name = "tsbtnGuestAdd";
            this.tsbtnGuestAdd.Size = new System.Drawing.Size(60, 96);
            this.tsbtnGuestAdd.Text = "入住登记";
            this.tsbtnGuestAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnGuestAdd.Click += new System.EventHandler(this.tsbtnGuestAdd_Click);
            // 
            // tsmiGuestInfo
            // 
            this.tsmiGuestInfo.Image = global::HotelManager.UI.Properties.Resources.gif_47_106;
            this.tsmiGuestInfo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiGuestInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsmiGuestInfo.Name = "tsmiGuestInfo";
            this.tsmiGuestInfo.Size = new System.Drawing.Size(60, 96);
            this.tsmiGuestInfo.Text = "顾客查询";
            this.tsmiGuestInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsmiGuestInfo.Click += new System.EventHandler(this.tsmiGuestInfo_Click);
            // 
            // tsbtnLock
            // 
            this.tsbtnLock.Image = global::HotelManager.UI.Properties.Resources.gif_47_035;
            this.tsbtnLock.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnLock.Name = "tsbtnLock";
            this.tsbtnLock.Size = new System.Drawing.Size(52, 96);
            this.tsbtnLock.Text = "锁定";
            this.tsbtnLock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnLock.Click += new System.EventHandler(this.tsbtnLock_Click);
            // 
            // tsbtnExit
            // 
            this.tsbtnExit.Image = global::HotelManager.UI.Properties.Resources.叉叉;
            this.tsbtnExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnExit.Name = "tsbtnExit";
            this.tsbtnExit.Size = new System.Drawing.Size(52, 96);
            this.tsbtnExit.Text = "退出";
            this.tsbtnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnExit.Click += new System.EventHandler(this.tsbtnExit_Click);
            // 
            // tsState
            // 
            this.tsState.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tsState.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tsTime,
            this.toolStripSeparator2,
            this.tslblDayOfWeek,
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.tsLoginId});
            this.tsState.Location = new System.Drawing.Point(0, 621);
            this.tsState.Name = "tsState";
            this.tsState.Size = new System.Drawing.Size(1030, 25);
            this.tsState.TabIndex = 2;
            this.tsState.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(68, 22);
            this.toolStripLabel1.Text = "当前时间：";
            // 
            // tsTime
            // 
            this.tsTime.Name = "tsTime";
            this.tsTime.Size = new System.Drawing.Size(33, 22);
            this.tsTime.Text = "time";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tslblDayOfWeek
            // 
            this.tslblDayOfWeek.Name = "tslblDayOfWeek";
            this.tslblDayOfWeek.Size = new System.Drawing.Size(76, 22);
            this.tslblDayOfWeek.Text = "dayOfWeek";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(68, 22);
            this.toolStripLabel2.Text = "登陆用户：";
            // 
            // tsLoginId
            // 
            this.tsLoginId.Name = "tsLoginId";
            this.tsLoginId.Size = new System.Drawing.Size(49, 22);
            this.tsLoginId.Text = "loginId";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 124);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox4);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Size = new System.Drawing.Size(1030, 497);
            this.splitContainer1.SplitterDistance = 156;
            this.splitContainer1.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtlv);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtBad);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtFree);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtIn);
            this.groupBox2.Controls.Add(this.txttype);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtCount);
            this.groupBox2.Location = new System.Drawing.Point(15, 238);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(135, 248);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "房间统计";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "类  型：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "房间数：";
            // 
            // txtlv
            // 
            this.txtlv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.txtlv.Enabled = false;
            this.txtlv.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtlv.ForeColor = System.Drawing.Color.White;
            this.txtlv.Location = new System.Drawing.Point(73, 210);
            this.txtlv.Name = "txtlv";
            this.txtlv.Size = new System.Drawing.Size(54, 23);
            this.txtlv.TabIndex = 8;
            this.txtlv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "入  住：";
            // 
            // txtBad
            // 
            this.txtBad.BackColor = System.Drawing.Color.Yellow;
            this.txtBad.Enabled = false;
            this.txtBad.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtBad.ForeColor = System.Drawing.SystemColors.Window;
            this.txtBad.Location = new System.Drawing.Point(73, 169);
            this.txtBad.Name = "txtBad";
            this.txtBad.Size = new System.Drawing.Size(54, 23);
            this.txtBad.TabIndex = 8;
            this.txtBad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "空  闲：";
            // 
            // txtFree
            // 
            this.txtFree.BackColor = System.Drawing.Color.Lime;
            this.txtFree.Enabled = false;
            this.txtFree.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtFree.ForeColor = System.Drawing.SystemColors.Window;
            this.txtFree.Location = new System.Drawing.Point(73, 132);
            this.txtFree.Name = "txtFree";
            this.txtFree.Size = new System.Drawing.Size(54, 23);
            this.txtFree.TabIndex = 8;
            this.txtFree.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "维  修：";
            // 
            // txtIn
            // 
            this.txtIn.BackColor = System.Drawing.Color.Red;
            this.txtIn.Enabled = false;
            this.txtIn.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtIn.ForeColor = System.Drawing.SystemColors.Window;
            this.txtIn.Location = new System.Drawing.Point(73, 94);
            this.txtIn.Name = "txtIn";
            this.txtIn.Size = new System.Drawing.Size(54, 23);
            this.txtIn.TabIndex = 8;
            this.txtIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttype
            // 
            this.txttype.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txttype.Enabled = false;
            this.txttype.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txttype.ForeColor = System.Drawing.Color.White;
            this.txttype.Location = new System.Drawing.Point(73, 29);
            this.txttype.Name = "txttype";
            this.txttype.Size = new System.Drawing.Size(54, 21);
            this.txttype.TabIndex = 8;
            this.txttype.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "入住率：";
            // 
            // txtCount
            // 
            this.txtCount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtCount.Enabled = false;
            this.txtCount.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtCount.ForeColor = System.Drawing.Color.White;
            this.txtCount.Location = new System.Drawing.Point(73, 60);
            this.txtCount.Name = "txtCount";
            this.txtCount.Size = new System.Drawing.Size(54, 23);
            this.txtCount.TabIndex = 8;
            this.txtCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tvRoom);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(141, 208);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "房间类型列表";
            // 
            // tvRoom
            // 
            this.tvRoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvRoom.Location = new System.Drawing.Point(3, 17);
            this.tvRoom.Name = "tvRoom";
            this.tvRoom.Size = new System.Drawing.Size(135, 188);
            this.tvRoom.TabIndex = 0;
            this.tvRoom.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvRoom_AfterSelect);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.lvRooms);
            this.groupBox4.Location = new System.Drawing.Point(12, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(846, 380);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "状态图";
            // 
            // lvRooms
            // 
            this.lvRooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvRooms.LargeImageList = this.imageList;
            this.lvRooms.Location = new System.Drawing.Point(3, 17);
            this.lvRooms.Name = "lvRooms";
            this.lvRooms.Size = new System.Drawing.Size(840, 360);
            this.lvRooms.TabIndex = 5;
            this.lvRooms.UseCompatibleStateImageBehavior = false;
            this.lvRooms.SelectedIndexChanged += new System.EventHandler(this.lvRooms_SelectedIndexChanged);
            this.lvRooms.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lvRooms_MouseMove);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "weizhu.jpg");
            this.imageList.Images.SetKeyName(1, "yizhu.jpg");
            this.imageList.Images.SetKeyName(2, "huai.jpg");
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.btnOk);
            this.groupBox3.Controls.Add(this.dgvRoom);
            this.groupBox3.Location = new System.Drawing.Point(6, 399);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(864, 95);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "订餐";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.txtList);
            this.groupBox5.Location = new System.Drawing.Point(647, 16);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(202, 48);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "清单";
            // 
            // txtList
            // 
            this.txtList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtList.Location = new System.Drawing.Point(6, 20);
            this.txtList.Multiline = true;
            this.txtList.Name = "txtList";
            this.txtList.Size = new System.Drawing.Size(193, 22);
            this.txtList.TabIndex = 5;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(777, 70);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "确定";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dgvRoom
            // 
            this.dgvRoom.AllowUserToAddRows = false;
            this.dgvRoom.AllowUserToDeleteRows = false;
            this.dgvRoom.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRoom.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRoom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRoom.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DishId,
            this.DishName,
            this.Price,
            this.unit,
            this.num,
            this.ch});
            this.dgvRoom.Location = new System.Drawing.Point(6, 23);
            this.dgvRoom.Name = "dgvRoom";
            this.dgvRoom.ReadOnly = true;
            this.dgvRoom.RowTemplate.Height = 23;
            this.dgvRoom.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRoom.Size = new System.Drawing.Size(614, 41);
            this.dgvRoom.TabIndex = 0;
            this.dgvRoom.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRoom_CellValueChanged);
            this.dgvRoom.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvRoom_CurrentCellDirtyStateChanged);
            // 
            // DishId
            // 
            this.DishId.DataPropertyName = "DishId";
            this.DishId.HeaderText = "ID";
            this.DishId.Name = "DishId";
            this.DishId.ReadOnly = true;
            // 
            // DishName
            // 
            this.DishName.DataPropertyName = "DishName";
            this.DishName.HeaderText = "名称";
            this.DishName.Name = "DishName";
            this.DishName.ReadOnly = true;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.HeaderText = "价格";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // unit
            // 
            this.unit.DataPropertyName = "Unit";
            this.unit.HeaderText = "单位";
            this.unit.Name = "unit";
            this.unit.ReadOnly = true;
            // 
            // num
            // 
            this.num.HeaderText = "数量";
            this.num.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.num.Name = "num";
            this.num.ReadOnly = true;
            // 
            // ch
            // 
            this.ch.FalseValue = "false";
            this.ch.HeaderText = "选择";
            this.ch.Name = "ch";
            this.ch.ReadOnly = true;
            this.ch.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ch.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ch.TrueValue = "true";
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 5000;
            this.toolTip.InitialDelay = 1000;
            this.toolTip.ReshowDelay = 100;
            this.toolTip.ToolTipTitle = "房间信息";
            // 
            // skinEngine
            // 
            this.skinEngine.@__DrawButtonFocusRectangle = true;
            this.skinEngine.DisabledButtonTextColor = System.Drawing.Color.Gray;
            this.skinEngine.DisabledMenuFontColor = System.Drawing.SystemColors.GrayText;
            this.skinEngine.InactiveCaptionColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.skinEngine.SerialNumber = "";
            this.skinEngine.SkinFile = null;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 646);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.tsState);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "FrmMain";
            this.Text = "酒店管理系统 V1.0";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_FormClosed);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.tsState.ResumeLayout(false);
            this.tsState.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoom)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem 菜单ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbout;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton tsbtnTypeMa;
        private System.Windows.Forms.ToolStripButton tsbtnInfoMa;
        private System.Windows.Forms.ToolStripButton tsbtnGuestAdd;
        private System.Windows.Forms.ToolStripButton tsmiGuestInfo;
        private System.Windows.Forms.ToolStripMenuItem tsmiUpdatePwd;
        private System.Windows.Forms.ToolStrip tsState;
        private System.Windows.Forms.ToolStripLabel tsTime;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel tsLoginId;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TreeView tvRoom;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtlv;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBad;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFree;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIn;
        private System.Windows.Forms.TextBox txttype;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCount;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ListView lvRooms;
        private System.Windows.Forms.ToolStripLabel tslblDayOfWeek;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgvRoom;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbtnStatistics;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtList;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ToolStripButton tsbtnLock;
        private System.Windows.Forms.ToolStripButton tsbtnExit;
        private System.Windows.Forms.DataGridViewTextBoxColumn DishId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DishName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private System.Windows.Forms.DataGridViewComboBoxColumn num;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ch;
        private System.Windows.Forms.ToolTip toolTip;
        private Sunisoft.IrisSkin.SkinEngine skinEngine;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotelManager.Model;
using HotelManager.BLL;
using System.Data.SqlClient;

namespace HotelManager.UI
{
    public partial class FrmChec : FrmBase
    {
        public FrmChec()
        {
            InitializeComponent();
            cboGender.SelectedIndex = 0;
        }

        //加载：
        private void FrmChec_Load(object sender, EventArgs e)
        {
            
            //查询未入住房间
            try
            {
                this.cboRoom.DisplayMember = "Description";
                this.cboRoom.ValueMember = "RoomId";
                List<Room> room = RoomManager.FreeRoom();
                for (int i = 0; i < room.Count; i++)
                {
                    room[i].Description = room[i].Description + "(床位:" + room[i].BedNum + ")";
                }
                room.Insert(0, new Room() { Description = "请选择" });
                this.cboRoom.DataSource = room;
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }

        //添加
        private void btnOk_Click(object sender, EventArgs e)
        {
            //非空验证
            if (string.IsNullOrEmpty(txtDeposit.Text.Trim()) || string.IsNullOrEmpty(txtGuestName.Text.Trim()) || string.IsNullOrEmpty(txtIdentityId.Text.Trim()) || string.IsNullOrEmpty(txtPhone.Text.Trim()))
            {
                MessageBox.Show("请完善顾客信息","提示",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            if (this.cboRoom.SelectedIndex == 0)
            {
                MessageBox.Show("请选择房间号", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            GuestRecord guest = new GuestRecord();
            guest.GuestName = this.txtGuestName.Text.Trim();
            guest.IdentityId = this.txtIdentityId.Text.Trim();
            guest.Phone = this.txtPhone.Text.Trim();
            guest.Gender = this.cboGender.Text;
            guest.Deposit = Convert.ToInt32(this.txtDeposit.Text.Trim());
            guest.RoomId = Convert.ToInt32(this.cboRoom.SelectedValue);
            guest.ResideDate = this.dtpResideDate.Value;
            try
            {
                if (GuestManager.AddGuest(guest))
                {
                    MessageBox.Show("添加成功!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }
    }
}

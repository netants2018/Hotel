﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotelManager.Common;

namespace HotelManager.UI
{
    public partial class FrmLock : FrmBase
    {
        public FrmLock()
        {
            InitializeComponent();
        }

        //解锁
        private void btnUnLock_Click(object sender, EventArgs e)
        {
            if (this.txtPassWord.Text == Variable.PassWord)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("密码输入错误！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FrmLock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.Alt)
            {
                e.Handled = true;
            }
        }
    }
}

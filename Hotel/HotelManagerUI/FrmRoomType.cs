﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotelManager.Model;
using HotelManager.BLL;
using System.Data.SqlClient;

namespace HotelManager.UI
{
    public partial class FrmRoomType : FrmBase
    {
        public FrmRoomType()
        {
            InitializeComponent();
            this.tsbtnCancel.Enabled = false;
            this.txtAddTypeName.Enabled = false;
            this.txtAddPrice.Enabled = false;
            this.btnAdd.Enabled = false;
        }

        //默认状态为新增
        private State state = State.add;

        //加载：
        private void FrmRoomTypeManager_Load(object sender, EventArgs e)
        {
            //绑定DataGridView数据
            try
            {
                this.dgvRoomTypeInfo.DataSource = TypeManager.GetAllRoomTypeInfo(this.txtTypeName.Text.Trim());
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message); ;
            }
            catch (Exception ex)
            {
                MessageBox.Show("未知异常：" + ex.Message);
            }
        }

        //新增：
        private void tsmiAdd_Click(object sender, EventArgs e)
        {
            state = State.add;
            this.txtAddTypeName.Text = string.Empty;
            this.txtAddPrice.Text = string.Empty;
            this.txtAddTypeName.Enabled = true;
            this.txtAddPrice.Enabled = true;
            this.btnAdd.Enabled = true;
            this.tsbtnCancel.Enabled = true;
            this.txtAddTypeName.Focus();
        }

        //修改：
        private void tsbtnUpdate_Click(object sender, EventArgs e)
        {
            state = State.update;
            this.txtAddPrice.Text = dgvRoomTypeInfo.CurrentRow.Cells["TypePrice"].Value.ToString();
            this.txtAddTypeName.Text = dgvRoomTypeInfo.CurrentRow.Cells["TypeName"].Value.ToString();
            this.tsbtnCancel.Enabled = true;
            this.txtAddTypeName.Enabled = true;
            this.txtAddPrice.Enabled = true;
            this.btnAdd.Enabled = true;
        }

        //删除：
        private void tsmiDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否删除?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            try
            {
                bool flag = TypeManager.DeleteRoomType(Convert.ToInt32(dgvRoomTypeInfo.CurrentRow.Cells["TypeId"].Value));
                if (flag)
                {
                    MessageBox.Show("删除成功!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //刷新
                    FrmRoomTypeManager_Load(null, null);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message); ;
            }
            catch (Exception ex)
            {
                MessageBox.Show("未知异常：" + ex.Message);
            }
        }

        //取消：
        private void tsbtnCancel_Click(object sender, EventArgs e)
        {
            tsmiAdd_Click(null, null);
            this.tsbtnCancel.Enabled = false;
            this.txtAddTypeName.Enabled = false;
            this.txtAddPrice.Enabled = false;
            this.btnAdd.Enabled = false;
        }

        //退出：
        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //查询：
        private void btnFind_Click(object sender, EventArgs e)
        {
            FrmRoomTypeManager_Load(null, null);
        }

        //确定：
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //非空验证
            if (string.IsNullOrEmpty(this.txtAddTypeName.Text) || string.IsNullOrEmpty(this.txtAddPrice.Text))
            {
                MessageBox.Show("请完善类型信息", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            #region 新增
            if (state == State.add)
            {
                try
                {
                    bool flag = TypeManager.AddRoomType(new RoomType(txtAddTypeName.Text.Trim(), Convert.ToDecimal(txtAddPrice.Text)));
                    if (flag)
                    {
                        MessageBox.Show("添加成功!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //刷新数据
                        FrmRoomTypeManager_Load(null, null);
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("数据库异常：" + ex.Message); ;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("未知异常：" + ex.Message);
                }
                return;
            }
            #endregion

            #region 更新
            if (state == State.update)
            {
                if (MessageBox.Show("是否修改?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
                try
                {
                    bool flag = TypeManager.UpdateRoomType(new RoomType(Convert.ToInt32(this.dgvRoomTypeInfo.CurrentRow.Cells["TypeId"].Value), txtAddTypeName.Text.Trim(), Convert.ToDecimal(txtAddPrice.Text)));
                    if (flag)
                    {
                        MessageBox.Show("修改成功!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //刷新数据
                        FrmRoomTypeManager_Load(null, null);
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("数据库异常：" + ex.Message); ;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("未知异常：" + ex.Message);
                }
                return;
            }
            #endregion
        }

        //选中内容发生改变
        private void dgvRoomTypeInfo_SelectionChanged(object sender, EventArgs e)
        {
            if (state == State.update)
            {
                this.txtAddPrice.Text = dgvRoomTypeInfo.CurrentRow.Cells["TypePrice"].Value.ToString();
                this.txtAddTypeName.Text = dgvRoomTypeInfo.CurrentRow.Cells["TypeName"].Value.ToString();
            }
        }
    }
}

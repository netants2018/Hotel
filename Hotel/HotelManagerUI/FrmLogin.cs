﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotelManager.Model;
using HotelManager.BLL;
using System.Data.SqlClient;
using HotelManager.Common;

namespace HotelManager.UI
{
    public partial class FrmLogin : FrmBase
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 登陆
        /// </summary>
        private void Login()
        {
            //非空验证
            if (string.IsNullOrEmpty(this.txtLoginId.Text.Trim()))
            {
                toolTip.Show("请输入用户名！", this.txtLoginId,1000);
                this.txtLoginId.Focus();
                return;
            }
            if (string.IsNullOrEmpty(this.txtPwd.Text))
            {
                toolTip.Show("请输入密码！", this.txtPwd, 1000);
                this.txtPwd.Focus();
                return;
            }
            //登陆
            try
            {
                bool flag = AdminManager.CheckIdPwd(new Admin(this.txtLoginId.Text, this.txtPwd.Text));
                if (flag)
                {
                    //保存登陆名及密码
                    Variable.LoginId = this.txtLoginId.Text.Trim();
                    Variable.PassWord = this.txtPwd.Text;
                    FrmMain main = new FrmMain();
                    main.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("用户名或密码错误", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }

        private void pbLogin_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            pbLogin_Click(null, null);
        }

    }
}

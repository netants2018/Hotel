﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotelManager.Model;
using HotelManager.BLL;
using System.Data.SqlClient;

namespace HotelManager.UI
{
    public partial class FrmRoomInfo : FrmBase
    {
        public FrmRoomInfo()
        {
            InitializeComponent();
            this.dgvRoomInfo.AutoGenerateColumns = false;
            this.tsbtnCancel.Enabled = false;
            this.txtBedNum.Enabled = false;
            this.txtDescription.Enabled = false;
            this.cboRoomType.Enabled = false;
            this.cboRoomState.Enabled = false;
            this.btnOk.Enabled = false;
        }

        //当前操作状态
        private State state = State.add;

        //加载：
        private void FrmRoomInfo_Load(object sender, EventArgs e)
        {
            InitRoomInfo(0);

            #region 绑定房间类型
            this.cboRoomType.DisplayMember = "TypeName";
            this.cboRoomType.ValueMember = "TypeId";
            try
            {
                List<RoomType> r = TypeManager.GetAllRoomTypeInfo("");
                r.Insert(0, new RoomType(0, "请选择", 0));
                this.cboRoomType.DataSource = r;
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
            #endregion

            #region 绑定房间状态
            this.cboRoomState.DisplayMember = "RoomStateName";
            this.cboRoomState.ValueMember = "RoomStateId";
            try
            {
                List<RoomState> roomState = RoomStateManager.GetRoomState();
                roomState.Insert(0, new RoomState() { RoomStateId = -1, RoomStateName = "请选择" });
                this.cboRoomState.DataSource = roomState;
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
            #endregion
        }

        //查询：
        private void btnFind_Click(object sender, EventArgs e)
        {
            int roomId = 0;
            if (!string.IsNullOrEmpty(this.txtRoomId.Text))
            {
                roomId = Convert.ToInt32(this.txtRoomId.Text);
            }
            InitRoomInfo(roomId);
        }

        //新增
        private void tsmiAdd_Click(object sender, EventArgs e)
        {
            state = State.add;
            this.txtBedNum.Enabled = true;
            this.txtDescription.Enabled = true;
            this.cboRoomType.Enabled = true;
            this.cboRoomState.Enabled = true;
            this.tsbtnCancel.Enabled = true;
            this.btnOk.Enabled = true;
        }

        //修改
        private void tsbtnUpdate_Click(object sender, EventArgs e)
        {
            state = State.update;
            this.txtBedNum.Text = dgvRoomInfo.CurrentRow.Cells["BedNum"].Value.ToString();
            this.txtDescription.Text = dgvRoomInfo.CurrentRow.Cells["Description"].Value.ToString();
            this.cboRoomType.Text = dgvRoomInfo.CurrentRow.Cells["TypeName"].Value.ToString();
            this.cboRoomState.Text = dgvRoomInfo.CurrentRow.Cells["RoomStateName"].Value.ToString();
            this.txtBedNum.Enabled = true;
            this.txtDescription.Enabled = true;
            this.cboRoomType.Enabled = true;
            this.cboRoomState.Enabled = true;
            this.tsbtnCancel.Enabled = true;
            this.btnOk.Enabled = true;
        }

        //删除
        private void tsmiDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否删除?", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.Cancel)
            {
                return;
            }
            try
            {
                bool flag = RoomManager.DelRoomInfo(Convert.ToInt32(dgvRoomInfo.CurrentRow.Cells["RoomId"].Value));
                if (flag)
                {
                    MessageBox.Show("删除成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //刷新
                    InitRoomInfo(0);
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }

        }

        //取消
        private void tsbtnCancel_Click(object sender, EventArgs e)
        {
            state = State.add;
            this.txtDescription.Text = string.Empty;
            this.txtBedNum.Text = string.Empty;
            this.tsbtnCancel.Enabled = false;
            this.txtBedNum.Enabled = false;
            this.txtDescription.Enabled = false;
            this.cboRoomType.Enabled = false;
            this.cboRoomState.Enabled = false;
            this.btnOk.Enabled = false;
        }

        //退出
        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //选中的内容发生改变：
        private void dgvRoomInfo_SelectionChanged(object sender, EventArgs e)
        {
            if (state == State.update)
            {
                this.txtBedNum.Text = dgvRoomInfo.CurrentRow.Cells["BedNum"].Value.ToString();
                this.txtDescription.Text = dgvRoomInfo.CurrentRow.Cells["Description"].Value.ToString();
                this.cboRoomType.Text = dgvRoomInfo.CurrentRow.Cells["TypeName"].Value.ToString();
                this.cboRoomState.Text = dgvRoomInfo.CurrentRow.Cells["RoomStateName"].Value.ToString();
            }
        }

        //确定：
        private void btnOk_Click(object sender, EventArgs e)
        {
            //非空验证
            if (string.IsNullOrEmpty(txtBedNum.Text.Trim()) || string.IsNullOrEmpty(txtDescription.Text.Trim()) || this.cboRoomType.SelectedIndex == 0 || this.cboRoomState.SelectedIndex == 0)
            {
                MessageBox.Show("请完善信息","提示",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }

            
            #region 新增
            if (state == State.add)
            {
                RoomBusiness room = new RoomBusiness();
                room.BedNum = Convert.ToInt32(txtBedNum.Text.Trim());
                room.Description = txtDescription.Text.Trim();
                room.RoomTypeId = Convert.ToInt32(this.cboRoomType.SelectedValue);
                room.RoomStateId = Convert.ToInt32(this.cboRoomState.SelectedValue);
                try
                {
                    bool flag = RoomManager.AddRoomInfo(room);
                    if (flag)
                    {
                        MessageBox.Show("添加成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //刷新
                        InitRoomInfo(0);
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("数据库异常：" + ex.Message);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("其它异常：" + ex.Message);
                }
            }
            #endregion

            #region 修改
            if (state == State.update)
            {
                RoomBusiness room = new RoomBusiness();
                room.BedNum = Convert.ToInt32(txtBedNum.Text.Trim());
                room.Description = txtDescription.Text.Trim();
                room.RoomTypeId = Convert.ToInt32(this.cboRoomType.SelectedValue);
                room.RoomId = Convert.ToInt32(dgvRoomInfo.CurrentRow.Cells["RoomId"].Value);
                room.RoomStateId = Convert.ToInt32(this.cboRoomState.SelectedValue);
                try
                {
                    bool flag = RoomManager.UpdateRoomInfo(room);
                    if (flag)
                    {
                        MessageBox.Show("更新成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //刷新
                        InitRoomInfo(0);
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("数据库异常：" + ex.Message);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("其它异常：" + ex.Message);
                }
            }
            #endregion
        }

        /// <summary>
        /// 查询全部房间信息
        /// </summary>
        private void InitRoomInfo(int roomId)
        {
            try
            {
                this.dgvRoomInfo.DataSource = RoomManager.GetRoomInfo(roomId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


    }
}

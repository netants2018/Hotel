﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotelManager.Model;
using HotelManager.BLL;
using System.Data.SqlClient;
using HotelManager.Common;

namespace HotelManager.UI
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            this.dgvRoom.AutoGenerateColumns = false;
            this.dgvRoom.ReadOnly = false;
            this.btnOk.Enabled = false;
            this.dgvRoom.Enabled = false;
            this.txtList.Enabled = false;
            skinEngine.SkinFile = Application.StartupPath + @"\\DiamondBlue.ssk";
        }

        //总金额
        private decimal money = 0;
        //房间号
        private int roomId = 0;

        //窗体关闭：
        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        //退出：
        private void tsmiExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //关于：
        private void tsmiAbout_Click(object sender, EventArgs e)
        {

        }

        //房间类型管理：
        private void tsbtnTypeMa_Click(object sender, EventArgs e)
        {
            FrmRoomType type = new FrmRoomType();
            type.ShowDialog();
            //刷新
            FrmMain_Load(null, null);
        }

        //房间信息管理：
        private void tsbtnInfoMa_Click(object sender, EventArgs e)
        {
            FrmRoomInfo info = new FrmRoomInfo();
            info.ShowDialog();
            //刷新
            FrmMain_Load(null, null);
        }

        //入住登记：
        private void tsbtnGuestAdd_Click(object sender, EventArgs e)
        {
            FrmChec chec = new FrmChec();
            chec.ShowDialog();
            //刷新
            FrmMain_Load(null, null);
        }

        //顾客信息查询：
        private void tsmiGuestInfo_Click(object sender, EventArgs e)
        {
            FrmGuestInfo guest = new FrmGuestInfo();
            guest.ShowDialog();
            //刷新
            FrmMain_Load(null, null);
        }

        //修改密码：
        private void tsmiUpdatePwd_Click(object sender, EventArgs e)
        {
            new FrmAdmin().ShowDialog();
        }

        //加载：
        private void FrmMain_Load(object sender, EventArgs e)
        {
            //设置工具栏信息
            tsTime.Text = DateTime.Now.ToString();
            this.tsLoginId.Text = Variable.LoginId;
            string strWeek = "星期" + "日一二三四五六".Substring((int)System.DateTime.Now.DayOfWeek, 1);
            this.tslblDayOfWeek.Text = strWeek;

            //绑定TreeView
            this.tvRoom.Nodes.Clear();
            TreeNode root = new TreeNode("全部");
            try
            {
                List<RoomType> roomType = RoomManager.GetAllRoomType();
                foreach (RoomType var in roomType)
                {
                    TreeNode node = new TreeNode();
                    node.Text = var.TypeName;
                    node.Tag = var.TypeId;
                    root.Nodes.Add(node);
                }
                this.tvRoom.Nodes.Add(root);
                this.tvRoom.ExpandAll();
                //显示所有房间状态
                this.lvRooms.Items.Clear();
                List<Room> room = RoomManager.GetRoomState();
                Method(room);
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
            //房间统计
            RoomCount("全部", 0);
        }

        //时间：
        private void timer_Tick(object sender, EventArgs e)
        {
            tsTime.Text = DateTime.Now.ToString();
        }

        //选中内容发生改变：
        private void tvRoom_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.lvRooms.Items.Clear();
            if (this.tvRoom.SelectedNode.Level == 0)
            {
                //显示所有房间状态
                List<Room> room = RoomManager.GetRoomState();
                Method(room);
            }
            try
            {
                //显示当前类型房间状态
                List<Room> room = RoomManager.GetRoomStateByType(Convert.ToInt32(this.tvRoom.SelectedNode.Tag));
                Method(room);
                //统计当前类型房间信息
                RoomCount(this.tvRoom.SelectedNode.Text, Convert.ToInt32(this.tvRoom.SelectedNode.Tag));
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }

        //锁定：
        private void tsbtnLock_Click(object sender, EventArgs e)
        {
            FrmLock frmLock = new FrmLock();
            frmLock.ShowInTaskbar = false;
            frmLock.ShowDialog();
        }

        //退出：
        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// 图片显示房间当前状态
        /// </summary>
        /// <param name="room"></param>
        private void Method(List<Room> room)
        {
            foreach (Room var in room)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = var.RoomId.ToString();
                if (var.RoomStateId == 1)
                {
                    lvi.ImageIndex = 1;
                }
                else if (var.RoomStateId == 2)
                {
                    lvi.ImageIndex = 0;
                }
                else
                {
                    lvi.ImageIndex = 2;
                }
                this.lvRooms.Items.Add(lvi);
            }
        }

        //收入统计：
        private void tsbtnStatistics_Click(object sender, EventArgs e)
        {
            FrmStatistics statistics = new FrmStatistics();
            statistics.ShowDialog();
        }

        //餐饮维护：
        private void tsbtnManager_Click(object sender, EventArgs e)
        {
            FrmDish dish = new FrmDish();
            dish.ShowDialog();
            FrmMain_Load(null, null);

        }

        //ListView选中内容改变：
        private void lvRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dgvRoom.DataSource = null;
            if (this.lvRooms.SelectedItems.Count == 0)
            {
                this.btnOk.Enabled = false;
                this.dgvRoom.Enabled = false;
                this.txtList.Enabled = false;
                this.txtList.Text = string.Empty;
                return;
            }
            if (this.lvRooms.SelectedItems[0].ImageIndex == 1)
            {
                //记录房间号
                this.roomId = Convert.ToInt32(this.lvRooms.SelectedItems[0].Text);
                //获取点餐信息
                try
                {
                    this.dgvRoom.DataSource = DishListManager.GetList();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("数据库异常：" + ex.Message);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("其它异常：" + ex.Message);
                }
                this.btnOk.Enabled = true;
                this.dgvRoom.Enabled = true;
                this.txtList.Enabled = true;
            }
            else
            {
                this.btnOk.Enabled = false;
                this.dgvRoom.Enabled = false;
                this.txtList.Text = string.Empty;
            }
        }


        //提交未提交控件的更改：
        private void dgvRoom_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvRoom.IsCurrentCellDirty)
            {
                dgvRoom.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        //单元格值更改：
        private void dgvRoom_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && !dgvRoom.Rows[e.RowIndex].IsNewRow)
            {
                if (e.ColumnIndex == 5)
                {
                    money = 0;
                    StringBuilder sb = new StringBuilder();
                    foreach (DataGridViewRow row in dgvRoom.Rows)
                    {
                        DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)row.Cells["ch"];
                        //选中状态
                        bool flag = Convert.ToBoolean(cell.Value);
                        if (flag)
                        {
                            //份数
                            int num = Convert.ToInt32(row.Cells["num"].Value);
                            //单价
                            decimal price = Convert.ToDecimal(row.Cells["Price"].Value);
                            if (num != 0)
                            {
                                sb.AppendLine(row.Cells["DishName"].Value.ToString() + " " + num.ToString() + row.Cells["Unit"].Value.ToString() + " ￥" + price * num);
                                money += Convert.ToDecimal(row.Cells["Price"].Value) * num;
                                this.txtList.Text = sb.ToString();
                            }

                        }
                    }
                    sb.AppendLine("- - - - - - - - - - - - - - - -");
                    sb.AppendLine("总计：￥" + money);
                    this.txtList.Text = sb.ToString();
                    //设置TextBox焦点总在最后
                    this.txtList.Focus();
                    this.txtList.Select(this.txtList.TextLength, 0);
                    this.txtList.ScrollToCaret();
                }
            }
        }

        //餐饮结账：
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                DishListManager.InsertPrice(roomId, money);
                MessageBox.Show("点餐成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }

        }

        /// <summary>
        /// 房间统计
        /// </summary>
        /// <param name="name">类型名称</param>
        /// <param name="typeId">类型Id</param>
        private void RoomCount(string name, int typeId)
        {
            try
            {
                //类型
                this.txttype.Text = name;
                //总数
                double count = RoomManager.GetRoomCount(typeId);
                this.txtCount.Text = count.ToString();
                //入住
                double comin = RoomManager.GetRoomStateCount(1, typeId);
                this.txtIn.Text = comin.ToString();
                //空闲
                double free = RoomManager.GetRoomStateCount(2, typeId);
                this.txtFree.Text = free.ToString();
                //维修
                double bad = RoomManager.GetRoomStateCount(3, typeId);
                this.txtBad.Text = bad.ToString();
                //入住率 入住/(总数-维修)
                this.txtlv.Text = (comin / (count - bad)).ToString();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }

        private Point pointView = new Point(0, 0);//位置

        //鼠标指针移过时：
        private void lvRooms_MouseMove(object sender, MouseEventArgs e)
        {
             //指定位置的项
            ListViewItem lvi = this.lvRooms.GetItemAt(e.X + 20, e.Y + 20);

            if (lvi != null)
            {
                if (pointView.X != e.X + 20 || pointView.Y != e.Y + 20)//防止闪烁
                {
                    //根据房间号查询信息
                    try
                    {
                        List<RoomBusiness> rb = RoomManager.GetRoomInfo(Convert.ToInt32(lvi.Text));
                        //设置toolTip
                        toolTip.Show(("房间号：" + rb[0].RoomId + "\n床位数：" + rb[0].BedNum + "\n状态：" + rb[0].RoomStateName + "\n类型：" + rb[0].TypeName + "\n描述：" + rb[0].Description), lvRooms, new Point(e.X + 5, e.Y + 5), 2000);
                        pointView.X = e.X + 20;
                        pointView.Y = e.Y + 20;
                        toolTip.Active = true;

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
                else
                {
                    toolTip.Hide(lvRooms);
                    pointView = new Point(e.X + 20, e.Y + 20);
                }
            }
        }

    }
}

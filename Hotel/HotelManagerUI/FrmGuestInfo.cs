﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotelManager.BLL;
using System.Data.SqlClient;
using HotelManager.Model;
using System.IO;
using HotelManager.Common;

namespace HotelManager.UI
{
    public partial class FrmGuestInfo : FrmBase
    {
        public FrmGuestInfo()
        {
            InitializeComponent();
            this.dgvGuestInfo.AutoGenerateColumns = false;
            this.dgvCus.AutoGenerateColumns = false;
        }

        //交易号
        private string tradeNo = string.Empty;

        //加载：
        private void FrmGuestInfo_Load(object sender, EventArgs e)
        {
            //查询所有未付款客户
            try
            {
                this.dgvGuestInfo.DataSource = GuestManager.GetAllGuest();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }

        //根据时间查询未付款客户：
        private void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime sDate = this.dtpCong.Value;
                DateTime eDate = this.dtpZhi.Value;
                this.dgvGuestInfo.DataSource = GuestManager.GetAllGuest(sDate, eDate);
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }

        //选中内容发生改变时：
        private void dgvGuestInfo_SelectionChanged(object sender, EventArgs e)
        {
            //房款
            decimal money = 0;
            //订金
            decimal deposit = 0;
            //消费
            decimal price = 0;
            if (this.dgvGuestInfo.SelectedRows.Count == 0)
            {
                return;
            }
            int roomId = Convert.ToInt32(dgvGuestInfo.CurrentRow.Cells["RoomId"].Value);
            DateTime resideDate = Convert.ToDateTime(dgvGuestInfo.CurrentRow.Cells["ResideDate"].Value);

            //查询餐饮消费金额
            price = Convert.ToDecimal(this.dgvGuestInfo.CurrentRow.Cells["DishPrice"].Value);
            this.txtTrade.Text = price.ToString();
            //查询房款
            try
            {
                money = GuestManager.PayMoney(roomId, resideDate);
                this.txtTotalMoney.Text = money.ToString();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
            //订金
            deposit = Convert.ToDecimal(this.dgvGuestInfo.CurrentRow.Cells["Deposit"].Value);
            this.txtDeposit.Text = deposit.ToString();
            //应付
            this.txtPay.Text = (money + price - deposit).ToString();

        }

        //确定退房：
        private void btnOk_Click(object sender, EventArgs e)
        {
            GettradeNo();
            if (this.dgvGuestInfo.SelectedRows.Count == 0)
            {
                return;
            }
            GuestRecord gr = new GuestRecord();
            //总价
            gr.TotalMoney = Convert.ToDecimal(this.txtTotalMoney.Text);
            gr.LeaveDate = DateTime.Now;
            gr.RoomId = Convert.ToInt32(dgvGuestInfo.CurrentRow.Cells["RoomId"].Value);
            gr.TradeNo = tradeNo;
            try
            {
                bool flag = GuestManager.UpdateState(gr);
                if (flag)
                {
                    if (MessageBox.Show("退房成功!是否打印发票?", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        //打印发票
                        Print();
                        MessageBox.Show("打印成功!");
                    }
                    //刷新
                    this.dgvGuestInfo.DataSource = GuestManager.GetAllGuest();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }

        //选项卡改变后：
        private void tcNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControl.SelectedTab == tabControl.TabPages[0])
            {
                //查询所有未付款客户
                try
                {
                    this.dgvGuestInfo.DataSource = GuestManager.GetAllGuest();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("数据库异常：" + ex.Message);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("其它异常：" + ex.Message);
                }
            }

            if (this.tabControl.SelectedTab == tabControl.TabPages[1])
            {
                //查询所有付款客户信息
                try
                {
                    this.dgvCus.DataSource = GuestManager.GetCusInfo("");
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("数据库异常：" + ex.Message);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("其它异常：" + ex.Message);
                }
            }

        }

        //查询付款客户：
        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                this.dgvCus.DataSource = GuestManager.GetCusInfo(this.txtName.Text.Trim());
            }
            catch (SqlException ex)
            {
                MessageBox.Show("数据库异常：" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("其它异常：" + ex.Message);
            }
        }

        /// <summary>
        /// 打印发票
        /// </summary>
        private void Print()
        {
            using (FileStream fs = new FileStream(tradeNo + ".txt", FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("**********************************");
                    sw.WriteLine("***       XXX酒店住房发票      ***");
                    sw.WriteLine("**********************************");
                    sw.WriteLine();
                    sw.WriteLine("客户姓名：" + this.dgvGuestInfo.CurrentRow.Cells["GuestName"].Value.ToString());
                    sw.WriteLine("入住日期：" + this.dgvGuestInfo.CurrentRow.Cells["ResideDate"].Value.ToString());
                    sw.WriteLine("退房日期：" + DateTime.Now.ToString());
                    sw.WriteLine("房款：" + this.txtTotalMoney.Text);
                    sw.WriteLine("消费：" + this.txtTrade.Text);
                    sw.WriteLine("押金：" + this.txtDeposit.Text);
                    sw.WriteLine("实收：" + this.txtPay.Text);
                    sw.WriteLine("操作员：" + Variable.LoginId);
                    sw.WriteLine("交易号：" + tradeNo);
                    sw.WriteLine("**********************************");
                }
            }
        }

        /// <summary>
        /// 生成交易号
        /// </summary>
        /// <returns></returns>
        private void GettradeNo()
        {
            Random random = new Random();
            tradeNo = random.Next().ToString();
        }

    }
}
